package com.progressoft.utilities.impl;

import com.progressoft.utilities.MatrixOperationsUtility;
import java.util.Scanner;


public class MatrixOperationsUtilityImpl implements MatrixOperationsUtility{

    
    
    
    /*--------- Region For Add Two Matrix ---------*/
    
    @Override
    public int[][] add(int[][] a, int[][] b) {
                      
       int m = a.length; 
       int n = a[0].length;
       int sum[][] = new int[m][n];
        
       for ( int c = 0 ; c < m ; c++ ){
        for ( int d = 0 ; d < n ; d++ ){            
           sum[c][d] = a[c][d] + b[c][d]; 
        }
       }       
      
      System.out.println("The Sum of Two matrix is : ");
      for ( int c = 0 ; c < m ; c++ ){
           for ( int d = 0 ; d < n ; d++ ){
              System.out.print(sum[c][d]+"\t");
           }
           System.out.println();
        } 
        return sum;
    }

    
    
    
    
    /*--------- Region For Scalar Multiply of Matrix ---------*/
    
    @Override
    public int[][] scalarMultiply(int scalar, int[][] a) {
        
      Scanner in = new Scanner(System.in);  
      System.out.println("Enter a number for scalar multiple :-");
      int x= in.nextInt();      
        
      int m=a.length; 
      int n= a[0].length;  
      int scalarMultiply[][] = new int[n][m]; 
      
      for ( int c = 0 ; c < m ; c++ ){
         for ( int d = 0 ; d < n ; d++ ){               
            scalarMultiply[d][c] = a[c][d];
         }
      }
 
      System.out.println("Result after Scalar Multiplication :-");
 
      for ( int c = 0 ; c < n ; c++ ){
         for ( int d = 0 ; d < m ; d++ ){
               scalarMultiply[c][d] *= x;
               System.out.print(scalarMultiply[c][d]+"\t");
         }
         System.out.print("\n");
      } 
      return scalarMultiply;
    }
    
    
    
    
        
    /*--------- Region For Transport of Matrix ---------*/

    @Override
    public int[][] transport(int[][] a) {
        
      int m=a.length; 
      int n=a[0].length;  
      int transpose[][] = new int[n][m]; 
      
      for ( int c = 0 ; c < m ; c++ ){
         for ( int d = 0 ; d < n ; d++ ){               
            transpose[d][c] = a[c][d];
         }
      }
 
      System.out.println("Transpose of entered matrix:-");
 
      for ( int c = 0 ; c < n ; c++ )
      {
         for ( int d = 0 ; d < m ; d++ ){
               System.out.print(transpose[c][d]+"\t");
         }
         System.out.print("\n");
      } 
      return transpose;
    }

    
    
    
    
    /*--------- Region For Multiply Two Matrix ---------*/
    
    @Override
    public int[][] multiply(int[][] a, int[][] b) {
        
        
        int m=a.length; 
        int n=a[0].length, sum=0, q, p;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number of rows and columns of second matrix");
        p = in.nextInt();
        q = in.nextInt();
        
        int multiply[][] = new int[m][q];
        
        for ( int c = 0 ; c < m ; c++ )
         {
            for ( int d = 0 ; d < q ; d++ )
            {   
               for ( int k = 0 ; k < p ; k++ )
               {
                  sum = sum + a[c][k]*b[k][d];
               }
 
               multiply[c][d] = sum;
               sum = 0;
            }
         }
 
         System.out.println("Product of entered matrices:-");
 
         for ( int c = 0 ; c < m ; c++ )
         {
            for ( int d = 0 ; d < q ; d++ )
               System.out.print(multiply[c][d]+"\t");
 
            System.out.print("\n");
         }
        return multiply;
    }
    
}
