/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progressoft.utilities;

import com.progressoft.utilities.impl.MatrixOperationsUtilityImpl;
import java.util.Scanner;

/**
 *
 * @author Lord
 */
public class Application {
    
    public static void main(String[] args) {
      int m, n, c, d ;
      Scanner in = new Scanner(System.in);
      
      MatrixOperationsUtilityImpl calc = new MatrixOperationsUtilityImpl();
 
      System.out.println("Enter the number of rows and columns of matrix");
      m = in.nextInt();
      n  = in.nextInt();
 
      int first[][] = new int[m][n];
      int second[][] = new int[m][n];
      int sum[][] = new int[m][n];
      
      System.out.println("Enter the elements of first matrix"); 
      for (  c = 0 ; c < m ; c++ ){
         for ( d = 0 ; d < n ; d++ ){
            first[c][d] = in.nextInt();
         }
      }     
      
      System.out.println("Enter the elements of second matrix");      
      for ( c = 0 ; c < m ; c++ ){
         for ( d = 0 ; d < n ; d++ ){
            second[c][d] = in.nextInt();
         }
      }     
     
      
      System.out.println("Enter 1 -> Add Matrix");      
      System.out.println("Enter 2 -> Scalar Multiply of Matrix");      
      System.out.println("Enter 3 -> Transpose of Matrix");      
      System.out.println("Enter 4 -> Matrix Multiplication");
      System.out.println("Enter 5 -> Exit");
    
      String choice = in.next();
      
      switch(choice){
          case "1":
              sum = calc.add(first, second);
              break;              
          case "2":
              sum = calc.scalarMultiply(c, first);
              break;
          case "3":
              sum = calc.transport(first);
              break;
          case "4":
              sum = calc.multiply(first, second);
              break;
          case "5":
              System.exit(0);
              break;
      }
      
    }
    
     
}
